<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Sign Up Form</title>
</head>
<body>
    <h1>Buat Account Baru!</h1>
    <h3>Sign Up Form</h3>
    <form action="/welcomeName" method="POST">
        @csrf
        First Name: <br><br>
        <input type="text" name="nama"><br><br>
        Last Name: <br><br>
        <input type="text" name="namab"><br><br> 
        Gender: <br><br>
        <label><input type="radio" name="gender" id="">Male</label><br>
        <label><input type="radio" name="gender" id="">Female</label><br>
        <label><input type="radio" name="gender" id="">Other</label><br><br>
        <label for="nationality">Nationality:</label><br><br>
        <select name="nationality" id="">
        <option value="indonesian">Indonesian</option>
        <option value="mars">Planet Mars</option>
        <option value="bekasi">Planet Bekasi</option>
        <option value="empire">S*nda empire</option>
        </select>
        <br><br>
        Language Spoken:
        <br><br>
        <label><input type="checkbox" name="" id="">Bahasa Indonesia</label><br>
        <label><input type="checkbox" name="" id="">English</label><br>
        <label><input type="checkbox" name="" id="">Other</label><br>
        <br>
        Bio:
        <br><br>
        <Textarea name="bio" rows="10" cols="30"></Textarea>
        <br>
        <input type="Submit" name="" id="" value="Sign Up">
    </form>
</body>
</html>